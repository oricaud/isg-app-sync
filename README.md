###  React App with AppSync

----
The purpose of this repository is to learn how to use Amplify to generate the Schema, Mutations and Queries provided
from AppSync. We already have an API in AppSync named 
[ISG-POC-LYLE API](https://console.aws.amazon.com/appsync/home?region=us-east-1#/sydcnnt53jac7o3hx6ntby63ue/v1/home)
Today you will learn how to serve a local client application that communicates with an AWS AppSync API. 

### Requirements 
You must have [npm](https://www.npmjs.com/package/latest-version) installed. 

You also must have AWS Amplify Installed on your node_modules run `npm install -g @aws-amplify/cli --unsafe-perm=true`

## How to compile
Run `amplify config` and create a profile, it will ask your API and Secret Keys, enter those or create new keys at 
[AWS Security Credentials](https://console.aws.amazon.com/iam/home?region=us-east-1#/security_credentials)

Run `amplify init` will initialize your aws project and ask you a few questions
Run `amplify add codegen --apiId sydcnnt53jac7o3hx6ntby63ue` will generate your schema, queries, and mutations

If successful, you should have a graphQL folder with all of the goodies.

Run `npm install` then `npm start` then you should have a React Application with your GraphQL folder running.

Run `amplify codegen` to pull the latest changes from AppSync
----------------------------------

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
