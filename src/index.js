import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import * as serviceWorker from './serviceWorker';
import aws_config from './aws/aws-exports';
import {ApolloProvider} from 'react-apollo';
import {Rehydrated} from 'aws-appsync-react';
import AWSAppSyncClient from 'aws-appsync';
import {BrowserRouter as Router} from "react-router-dom";
import MyDrawer from "./components/drawer/MyDrawer";

const client = new AWSAppSyncClient({
    url: aws_config.aws_appsync_graphqlEndpoint,
    region: aws_config.aws_appsync_region,
    auth: {
        type: aws_config.aws_appsync_authenticationType,
        apiKey: aws_config.aws_appsync_apiKey
    },
    resolvers: {}
});

const routing = (
    <Router>
        <MyDrawer> </MyDrawer>
    </Router>
);

ReactDOM.render(
    <ApolloProvider client={client}>
        <Rehydrated>
            {routing}
        </Rehydrated>
    </ApolloProvider>, document.getElementById("root"));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
