import React from 'react';
import Amplify from '@aws-amplify/core'
import config from './aws/aws-exports'

export default class App extends React.PureComponent {

    constructor(props) {
        super(props);
        Amplify.configure(config);
    }

    render() {
        return (
            <div>
            </div>
        );
    }
}
