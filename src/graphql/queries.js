/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const allCustomers = `query AllCustomers {
  allCustomers {
    id
    advisorId
    agentId
    businessSolutionId
    agent {
      ID
      FirstName
      LastName
      DetachCode
      AppointmentDate
      OfficeCode
    }
    details {
      OwnerId
      FirstName
      LastName
    }
    policy {
      PolicyNum
      SellingProducerContractNum
      InsuredId
      ProductCd
      PolicyStatusCd
      EffectiveDate
      OwnerId
    }
  }
}
`;
export const customer = `query Customer($ownerId: String!) {
  customer(ownerId: $ownerId) {
    id
    advisorId
    agentId
    businessSolutionId
    agent {
      ID
      FirstName
      LastName
      DetachCode
      AppointmentDate
      OfficeCode
    }
    details {
      OwnerId
      FirstName
      LastName
    }
    policy {
      PolicyNum
      SellingProducerContractNum
      InsuredId
      ProductCd
      PolicyStatusCd
      EffectiveDate
      OwnerId
    }
  }
}
`;
export const allPolicies = `query AllPolicies {
  allPolicies {
    PolicyNum
    SellingProducerContractNum
    InsuredId
    ProductCd
    PolicyStatusCd
    EffectiveDate
    OwnerId
    customer {
      id
      advisorId
      agentId
      businessSolutionId
    }
    agent {
      ID
      FirstName
      LastName
      DetachCode
      AppointmentDate
      OfficeCode
    }
    productConnection {
      ID
      effective
      start
      end
      name
      policyId
    }
  }
}
`;
export const policy = `query Policy {
  policy {
    PolicyNum
    SellingProducerContractNum
    InsuredId
    ProductCd
    PolicyStatusCd
    EffectiveDate
    OwnerId
    customer {
      id
      advisorId
      agentId
      businessSolutionId
    }
    agent {
      ID
      FirstName
      LastName
      DetachCode
      AppointmentDate
      OfficeCode
    }
    productConnection {
      ID
      effective
      start
      end
      name
      policyId
    }
  }
}
`;
export const allAgents = `query AllAgents {
  allAgents {
    ID
    FirstName
    LastName
    DetachCode
    AppointmentDate
    OfficeCode
  }
}
`;
export const agent = `query Agent {
  agent {
    ID
    FirstName
    LastName
    DetachCode
    AppointmentDate
    OfficeCode
  }
}
`;
export const allProducts = `query AllProducts {
  allProducts {
    ID
    effective
    start
    end
    name
    policyId
  }
}
`;
export const product = `query Product {
  product {
    ID
    effective
    start
    end
    name
    policyId
  }
}
`;
export const allAdvisors = `query AllAdvisors {
  allAdvisors {
    ID
    firstName
    lastName
  }
}
`;
export const advisor = `query Advisor {
  advisor {
    ID
    firstName
    lastName
  }
}
`;
export const allCustomerDetails = `query AllCustomerDetails {
  allCustomerDetails {
    OwnerId
    FirstName
    LastName
    Address {
      AddrLine1
      AddrLine2
      City
      State
      ZIP
    }
  }
}
`;
export const getIsg = `query GetIsg {
  getIsg {
    policies {
      PolicyNum
      SellingProducerContractNum
      InsuredId
      ProductCd
      PolicyStatusCd
      EffectiveDate
      OwnerId
    }
    customers {
      id
      advisorId
      agentId
      businessSolutionId
    }
    agents {
      ID
      FirstName
      LastName
      DetachCode
      AppointmentDate
      OfficeCode
    }
  }
}
`;
