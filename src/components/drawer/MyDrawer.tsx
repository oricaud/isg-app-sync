import React from 'react';
import clsx from 'clsx';
import {useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PeopleIcon from '@material-ui/icons/Person';
import AccountIcon from '@material-ui/icons/AccountCircle';
import PolicyIcon from '@material-ui/icons/ListAlt';
import FolderIcon from '@material-ui/icons/Folder';
import ProductIcon from '@material-ui/icons/Ballot'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Routes from "../../routes/Routes";
import {NavLink} from "react-router-dom";
import {miniDrawerStyles} from "./MyDrawer-styles";
import {Button, Typography} from "@material-ui/core";

export default function MyDrawer() {
    const classes = miniDrawerStyles();
    const [open, setOpen] = React.useState(true);
    const theme = useTheme();

    function handleDrawerOpen() {
        setOpen(true);
        console.log('classes ' + JSON.stringify(classes));
    }

    function handleDrawerClose() {
        setOpen(false);
    }
    return (
        <div className={classes.root}>
            <CssBaseline />

            <AppBar position="fixed" className={clsx(classes.appBar, {[classes.appBarShift]: open,})}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}>
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" noWrap className={classes.title}>
                        ISG
                    </Typography>
                    <Button color="inherit">Login</Button>
                </Toolbar>
            </AppBar>
            <Drawer className={classes.drawer} variant="persistent" anchor="left" open={open}
                classes={{ paper: classes.drawerPaper}}>
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                    </IconButton>
                </div>
                <Divider className={classes.divider}/>

                <NavLink activeClassName="active" to="/advisors">
                    <ListItem button key="Advisors">
                        <ListItemIcon className={classes.itemIcon}>
                            <PeopleIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Advisors" classes={{primary: classes.categoryHeaderText}}/>
                    </ListItem>
                </NavLink>

                <NavLink activeClassName="active" to="/products">
                    <ListItem button key="Products">
                        <ListItemIcon className={classes.itemIcon}>
                            <ProductIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Products" classes={{primary: classes.categoryHeaderText}}/>
                    </ListItem>
                </NavLink>

                <NavLink activeClassName="active" to="/policies">
                    <ListItem button key="Policies">
                        <ListItemIcon className={classes.itemIcon}>
                            <PolicyIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Policies" classes={{primary: classes.categoryHeaderText}}/>
                    </ListItem>
                </NavLink>

                <Divider className={classes.divider}/>

                <NavLink activeClassName="active" to="/profile">
                    <ListItem button key="Profile">
                        <ListItemIcon className={classes.itemIcon}>
                            <AccountIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Profile" classes={{primary: classes.categoryHeaderText}}/>
                    </ListItem>
                </NavLink>

                <NavLink activeClassName="active" to="/api">
                    <ListItem button key="API">
                        <ListItemIcon className={classes.itemIcon}>
                            <FolderIcon/>
                        </ListItemIcon>
                        <ListItemText primary="API" classes={{primary: classes.categoryHeaderText}}/>
                    </ListItem>
                </NavLink>
            </Drawer>
            <main className={clsx(classes.content, { [classes.contentShift]: open })} >
                <Routes/>
            </main>
        </div>
    );
}
