import {makeStyles} from "@material-ui/core";


export const drawerWidth = 240;

export const miniDrawerStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },

    appBar: {
        backgroundColor: '#2d2c2c',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    table: {
        backgroundColor: '#0000'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
        color: 'white'
    },
    title: {
        flexGrow: 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        background: '#2d2c2c',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        marginTop: '35px',
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    divider: {
        backgroundColor: '#434343'
    },
    categoryHeaderText: {
        fontSize: 15,
        fontWeight: 500,
        color: theme.palette.common.white,
    },
    itemIcon: {
        margin: 0,
        color: 'white'
    },
    itemText: {
        fontSize: 14,
        fontWeight: 500,
    },
}));
