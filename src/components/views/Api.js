import React from "react";
import {Query} from "react-apollo";
import gql from "graphql-tag";
import * as queries from "../../graphql/queries";
import MyTable from "../table/Table";

class Api extends React.Component {
    render() {
        return (
            <>
                <Query query={gql(queries.allCustomers)} fetchPolicy="cache">
                    {({data: {allCustomers}, loading, error}) => {
                        if (error) return <h3>Error could not load Customer</h3>;
                        if (loading) return <h3>Loading customers...</h3>;
                        console.log('test' + JSON.stringify(allCustomers));
                        console.table(allCustomers);
                        return (
                            <MyTable tableName={'Customers'} tableData={allCustomers}/>
                        );
                    }}
                </Query>

                <Query query={gql(queries.allPolicies)} fetchPolicy="cache">
                    {({data: {allPolicies}, loading, error}) => {
                        if (error) return <h3>Error could not load Customer</h3>;
                        if (loading) return <h3>Loading customers...</h3>;
                        console.log('test' + JSON.stringify(allPolicies));
                        return (
                            <MyTable tableName={'Policies'} tableData={allPolicies}/>
                        );
                    }}
                </Query>

                <Query query={gql(queries.allAgents)} fetchPolicy="cache">
                    {({data: {allAgents}, loading, error}) => {
                        if (error) return <h3>Error could not load Customer</h3>;
                        if (loading) return <h3>Loading customers...</h3>;
                        console.log('test' + JSON.stringify(allAgents));
                        return (
                            <MyTable tableName={'Agents'} tableData={allAgents}/>
                        );
                    }}
                </Query>

                <Query query={gql(queries.allAdvisors)} fetchPolicy="cache">
                    {({data: {allAdvisors}, loading, error}) => {
                        if (error) return <h3>Error could not load Customer</h3>;
                        if (loading) return <h3>Loading customers...</h3>;
                        console.log('test' + JSON.stringify(allAdvisors));
                        return (
                            <MyTable tableName={'Advisors'} tableData={allAdvisors}/>
                        );
                    }}
                </Query>

                <Query query={gql(queries.allProducts)} fetchPolicy="cache">
                    {({data: {allProducts}, loading, error}) => {
                        if (error) return <h3>Error could not load Customer</h3>;
                        if (loading) return <h3>Loading customers...</h3>;
                        console.log('test' + JSON.stringify(allProducts));
                        return (
                            <MyTable tableName={'Products'} tableData={allProducts}/>
                        );
                    }}
                </Query>

            </>
        );
    }
}

export default Api;
