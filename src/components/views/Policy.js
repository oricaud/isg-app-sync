import React from "react";
import {Query} from "react-apollo";
import gql from "graphql-tag";
import {PolicyCard} from "../policy/PolicyCard";
import * as queries from "../../graphql/queries";

class Policy extends React.Component {
    render() {
        return (
            <div>
                <Query query={gql(queries.allPolicies)}>
                    {({ data, loading, error }) => {
                        if (error) return <h3>Error could not load Policies</h3>;
                        if (loading) return <h3>Loading customers...</h3>;
                        {
                            console.log('data' + JSON.stringify(data))
                        }
                        return (
                            <PolicyCard tableName={'Policies'} tableData={data.policies}/>
                        );
                    }}
                </Query>
            </div>
        );
    }
}

export default Policy;
