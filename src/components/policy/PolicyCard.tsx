import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import './PolicyCard.css';
import {Divider, makeStyles} from "@material-ui/core";
import termLifeAsset from '../../img/term-essential.jpg';
import universalAsset from '../../img/universal-life.jpg';
import variableAsset from '../../img/variable-life.jpg';

const useStyles = makeStyles({
    card: {
        maxWidth: 370,
        height: 350,
        color: '#ffff',
        background: '#2d2c2c',
    },
    content: {
        height: 160
    },
});

export const PolicyCard = (props) => {
    const classes = useStyles();
    const products = [
        {
            name: 'Term Life Insurance', img: termLifeAsset,
            description: 'Budget-friendly, temporary protection with conversion privileges. Prudential ' +
                'offers five different term life insurance policies.'
        },
        {
            name: 'Universal Life Insurance', img: universalAsset,
            description: 'Long-term or even permanent life insurance coverage. Prudential\'s UL policies ' +
                'offer death benefit protection, no-lapse guarantees, and the potential to build cash value.'
        },
        {
            name: 'Variable Life Insurance', img: variableAsset,
            description: 'Lifetime insurance protection that also offers growth and income potential.' +
                ' Prudential has three variable products designed to meet your clients\' wide-ranging needs. '
        },
    ];

    return (
        <>
            <div className="flex-container">
                {
                    products.map((items, index) => {

                        return (
                            <div>
                                <Card className={classes.card} key={index}>
                                    <CardActionArea>
                                        <CardMedia
                                            component="img"
                                            alt="Contemplative Reptile"
                                            height="140"
                                            src={items.img}
                                            title="Contemplative Reptile"
                                        />
                                        <CardContent className={classes.content}>
                                            <Typography gutterBottom variant="h5" component="h2">
                                                {items.name}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                {items.description}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                    <Divider/>
                                    <CardActions>
                                        <Button size="small" color="primary">
                                            Enroll
                                        </Button>
                                        <Button size="small" color="primary">
                                            Learn More
                                        </Button>
                                    </CardActions>
                                </Card>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
};

