import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './Table.css'

export default class MyTable extends React.PureComponent {

        render() {
            return (
                <>
                    <h1> {this.props.tableName} </h1>
                    <Paper className="Paper" boxshadow={3}>
                        <Table className="Table">
                            <TableHead>
                                {
                                    this.props.tableData.map((items, index) => {
                                        if (index <= 0) {
                                            return (
                                                <TableRow key={index}>
                                                    {Object.keys(items).map((key) => {
                                                        return (
                                                            <TableCell key={key}>
                                                                {key}
                                                            </TableCell>

                                                        )
                                                    })}
                                                </TableRow>
                                            )
                                        }
                                        return null;
                                    })
                                }
                            </TableHead>
                            <TableBody>
                                {
                                    this.props.tableData.map((items, index) => {
                                        return (
                                            <TableRow key={index}>
                                                {Object.keys(items).map((key) => {
                                                    return (
                                                        <TableCell key={key}>
                                                            {
                                                                JSON.stringify(items[key]).replace(/{|}/g, '')
                                                            }
                                                        </TableCell>
                                                    )
                                                })}
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </Paper>
                </>
            );
        }
}
