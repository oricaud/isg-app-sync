import React from "react";
import App from "../App";
import Advisor from "../components/views/Advisor";
import Product from "../components/views/Product";
import Policy from "../components/views/Policy";
import Profile from "../components/views/Profile";
import Api from "../components/views/Api";
import Notfound from "../components/views/NotFound";
import {Route, Switch} from "react-router-dom";

const Routes = () =>
    <div>
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/advisors" component={Advisor} />
            <Route path="/products" component={Product} />
            <Route path="/policies" component={Policy} />
            <Route path="/profile" component={Profile} />
            <Route path="/api" component={Api}/>
            <Route component={Notfound} />
        </Switch>
    </div>;

export default Routes
